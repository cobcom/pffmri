# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='pffmri',
    version='0.0.0',
    packages=['tests', 'pffmri'],
    url='https://gitlab.inria.fr/cobcom/pffmri.git',
    license='',
    author='Isa Costantini',
    author_email='isa.costantini@inria.fr',
    description='A Python package that implements paradigm-free functional '
                'magnetic resonance imaging.'
)
